import React from 'react'
import {View, Text, StyleSheet, TextInput, Image, TouchableOpacity} from 'react-native'

const LoginScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Image
             style={styles.tinyLogo}
             source={require('../../../assets/splashimage.png')}/>
            <View style={styles.conButton}>
                <Text style={styles.textsignin}>Sign In</Text>
                <View style={styles.formSignin}>
                    <Text style={styles.textForm}>Username</Text>
                    <TextInput style={styles.inputForm}></TextInput>
                </View>
                <View style={styles.formSignin}>
                    <Text style={styles.textForm}>Password</Text>
                    <TextInput style={styles.inputForm} secureTextEntry={true}></TextInput>
                </View>

                <View style={styles.conFooter}>
                    <TouchableOpacity style={styles.forgot}>
                        <Text style={styles.textForgot}>Forgot Password?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.loginFooter}
                    onPress={() => navigation.navigate('DrawerStackScreen')}>
                        <Text style={styles.textsignFooter}>Sign In</Text>
                    </TouchableOpacity>
                </View>
            </View>
         
        </View>
        
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    tinyLogo: {
        width: 250,
        height: 250,
        resizeMode: 'contain'
    },
    conButton: {
        width: '80%'
    },
    formSignin: {
        paddingVertical: 10
    },
    textsignin: {
        fontSize: 38,
        color: '#D81E28',
        marginVertical: 20
    },
    textForm: {
        fontSize: 16,
        marginBottom: 8,
        color: '#C4C4C4'
    },
    inputForm: {
        borderWidth:1,
        borderColor: '#C4C4C4',
        height: 50,
        paddingHorizontal: 8
    },
    conFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20
    },
    textForgot: {
        fontSize:16
    },
    textsignFooter: {
        color: '#D81E28',
        fontSize: 16,
        fontWeight: '700'
    }
})