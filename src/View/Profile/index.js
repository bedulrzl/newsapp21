import React from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';

class ProfileScreen extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.conProfile}>
                    <View style={styles.bagProfile}>
                        <Image
                        style={styles.profile}
                        source={require('../../../assets/profile.png')}/>
                    </View>
              
                    <Text style={styles.textProfile}>Aprijal</Text>
                    <Text style={{color:'#aaaaaa'}}>Mobile App Developer</Text>
                    <Text style={{color:'#aaaaaa'}}>Indonesia</Text>

                </View>

                <View style={{marginBottom:10}}>
                    <Text style={{fontSize:24}}>Social Media</Text>
                    <View style={styles.sosmedIcon}>
                        <View style={{alignItems:'center'}}>
                            <Ionicons name="logo-facebook" size={36} color="#D81E28" />
                            <Text>@Aprijal</Text>
                        </View>
                        <View style={{alignItems:'center'}}>
                            <Ionicons name="logo-twitter" size={36} color="#D81E28" />  
                            <Text>@Aprijal</Text>
                        </View>
                        <View style={{alignItems:'center'}}>
                            
                        <Ionicons name="logo-instagram" size={36} color="#D81E28" />
                            <Text>@Aprijal</Text>
                        </View>

                      
                       
                    </View>
                </View>
                <View>
                    <Text style={{fontSize:24}}>Portofolio</Text>
                    <View style={styles.sosmedIcon}>
                        <View style={{alignItems:'center'}}>
                          
                            <Ionicons name="logo-github" size={36} color="#D81E28" />
                            <Text>@bedulrzl</Text>
                        </View>
                        <View style={{alignItems:'center'}}>
                            <Ionicons name="logo-dribbble" size={36} color="#D81E28" />  
          
                            <Text>@Aprijal</Text>
                        </View>
                        <View style={{alignItems:'center'}}>
                            
                        <Ionicons name="logo-stackoverflow" size={36} color="#D81E28" />
                      
                            <Text>@Aprijal</Text>
                        </View>
                    </View>
                </View>

                <TouchableOpacity style={styles.btnLogout} onPress={() => this.props.navigation.navigate('Welcome')}>
                    <Text style={{color:'#fff', fontSize:18, fontWeight:'700'}}>Logout</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

export default ProfileScreen

const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginTop: 40,
        marginHorizontal: 16,     
    },
    conProfile: {
        paddingVertical: 30,
        alignItems:'center'
    },
    bagProfile: {
        backgroundColor:'#fff',
        borderRadius: 150,
        width: 200,
        height: 200,
        alignItems:'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    profile: {
        width: 150,
        height: 150,
        resizeMode: 'contain',
    },
    textProfile: {
        fontSize: 36,
        fontWeight:'700',
        color: '#D81E28'
    },
    sosmedIcon: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 20,
        marginTop:10,
        backgroundColor: '#fff',
        borderRadius: 20,
    },
    btnLogout: {
        marginVertical: 30,
        alignItems:'center',
        backgroundColor: '#D81E28',
        paddingVertical:16,
        paddingHorizontal: 16,
        borderRadius: 8
    }
})