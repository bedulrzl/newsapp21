import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native'


const welcomScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Image
             style={styles.tinyLogo}
             source={require('../../assets/splashimage.png')}/>
            <View style={styles.conButton}>
            <TouchableOpacity style={styles.signUp} 
                onPress={ ()=> navigation.navigate('Register')}>
                    <Text style={styles.textSignup}>Register</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.signIn}
            onPress={() => navigation.navigate('Login')}>
                    <Text style={styles.textSignin}>Log In</Text>
            </TouchableOpacity>
              
            </View>
         
        </View>
    )
}

export default welcomScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: 50
    },
    welcom: {
        fontSize: 36,
        color:'#fff'
    },
    tinyLogo: {
        width: 250,
        height: 250,
        resizeMode: 'contain'
      },
    conButton: {
        alignItems:'center',
        backgroundColor: '#ffffff',
        width:'100%',
        padding: 30,
        position: 'absolute',
        bottom: 0,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        
    },
    signIn: {
        backgroundColor: '#ffff',
        borderWidth: 2,
        borderColor: '#D81E28',
        borderRadius: 10,
        paddingHorizontal: 8,
        paddingVertical: 16,
        marginVertical: 10,
        width:'100%',
        alignItems: 'center'
    },
    textSignin: {
        color: '#D81E28',
        fontSize: 16,
        fontWeight: '700',
        letterSpacing: 0.5
    },
    signUp: {
        backgroundColor: '#D81E28',
        borderWidth: 2,
        borderColor: '#D81E28',
        borderRadius: 10,
        paddingHorizontal: 8,
        paddingVertical: 16,
        marginVertical: 10,
        width:'100%',
        alignItems: 'center'
    },
    textSignup: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '700',
        letterSpacing: 1
    }

})