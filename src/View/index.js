import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import 'react-native-gesture-handler';

import LoginScreen from './Login'
import RegisterScreen from './Register'
import HomeScreen from './Home'
import ProfileScreen from './Profile'
import SplashScreen from './Splash'
import welcomScreen from './Welcome'
import DetailScreen from './Home/Detail'
import { color } from 'react-native-reanimated';

const Stack = createStackNavigator();
const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();

const iconTab = ({ route }) => {
    return ({
        tabBarIcon: ({ focused, size, color }) => {
            if (route.name == 'Home') {
                return <Ionicons name={focused ? 'home' : 'home-outline'} size={size} color={color} />
            } else if (route.name == 'Profile') {
                return <Ionicons name={focused ? 'person-circle' : 'person-circle-outline'} size={size} color={color    }  />
            } 
        },
    });
  }

const TabsStackScreen = () => (
    <TabsStack.Navigator screenOptions={iconTab} tabBarOptions={{activeTintColor:'#D81E28', inactiveTintColor:'gray'}}>
  
        <TabsStack.Screen name='Home' component={HomeScreen}
            options={{
                title: 'Home',
          
            }} />
  
        <TabsStack.Screen name='Profile' component={ProfileScreen}
            options={{
                title: 'Profil'
            }} />
  
    </TabsStack.Navigator>
  );
  
  
  
  const DrawerStackScreen = () => (
    <DrawerStack.Navigator >
  
        <DrawerStack.Screen name='TabsStackScreen' component={TabsStackScreen}/>
  
    </DrawerStack.Navigator>
  );

export default class Appscr extends React.Component {
    render(){
        return(
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name='Splash' component={SplashScreen} options={{headerShown: false}}/>
                    <Stack.Screen name='Welcome' component={welcomScreen} options={{headerShown: false}}/>
                    <Stack.Screen name='Login' component={LoginScreen}/>
                    <Stack.Screen name='Register' component={RegisterScreen}/>
                    <Stack.Screen name='DrawerStackScreen' component={DrawerStackScreen} options={{headerShown: false}}/>
                    <Stack.Screen name='Detail' component={DetailScreen}/>
                    

                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}