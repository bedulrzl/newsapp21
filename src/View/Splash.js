import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.navigate("Welcome")
        }, 3000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
           <Image
        style={styles.tinyLogo}
        source={require('../../assets/splashimage.png')}
      />
            
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    tinyLogo: {
        width: 150,
        height: 150,
        resizeMode: 'contain'
      },
})
