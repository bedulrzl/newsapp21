import React from 'react'
import {View, Text, StyleSheet, TextInput, Image, TouchableOpacity, ScrollView} from 'react-native'

const RegisterScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
        <ScrollView style={styles.conScroll}>
            <Image
             style={styles.tinyLogo}
             source={require('../../../assets/splashimage.png')}/>
            
            <View style={styles.conButton}>
                <Text style={styles.textsignin}>Register</Text>
                <View style={styles.formSignin}>
                    <Text style={styles.textForm}>Name</Text>
                    <TextInput style={styles.inputForm}></TextInput>
                </View>
                <View style={styles.formSignin}>
                    <Text style={styles.textForm}>Email</Text>
                    <TextInput style={styles.inputForm}></TextInput>
                </View>
                <View style={styles.formSignin}>
                    <Text style={styles.textForm}>Phone Number</Text>
                    <TextInput style={styles.inputForm}></TextInput>
                </View>
                <View style={styles.formSignin}>
                    <Text style={styles.textForm}>Passowrd</Text>
                    <TextInput style={styles.inputForm} secureTextEntry={true}></TextInput>
                </View>

                <View style={styles.conFooter}>
                    <TouchableOpacity style={styles.forgot}>
                        <Text style={styles.textForgot}>Already have an account?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.loginFooter} onPress={() => navigation.navigate('Home')}>
                        <Text style={styles.textsignFooter}>Register</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
           
         
        </View>
        
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    conScroll: {
        width:'80%',
        flex: 1
    },
    tinyLogo: {
        width: 150,
        height: 150,
        resizeMode: 'contain'
    },
    conButton: {
        paddingTop: 10,
        paddingBottom: 20
    },
    formSignin: {
        paddingVertical: 5
    },
    textsignin: {
        fontSize: 38,
        color: '#D81E28',
        marginVertical: 10
    },
    textForm: {
        fontSize: 16,
        marginBottom: 8,
        color: '#C4C4C4'
    },
    inputForm: {
        borderWidth:1,
        borderColor: '#C4C4C4',
        height: 40,
        paddingHorizontal: 8
    },
    conFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20
    },
    textForgot: {
        fontSize:16
    },
    textsignFooter: {
        color: '#D81E28',
        fontSize: 16,
        fontWeight: '700'
    }
})