import React from 'react'
import {View, Text, StyleSheet, Image} from 'react-native'
import { WebView } from 'react-native-webview';

class DetailScreen extends React.Component {
    render() {   
      return (
          <WebView
        source={{ uri: this.props.route.params.urlLink }}
      />
       
      );
    }
}

export default DetailScreen;
