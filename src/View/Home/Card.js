import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import moment from "moment";

export default class Card extends React.Component {

  render() {
    const data = this.props.data;
    var dateNews = moment(data.publishedAt).format("h:mm a");
    console.log(dateNews);

    return (
      <View style={styles.container}>
        <View style={styles.conCard}>
          <Image style={styles.imageCard} source={{ uri: data.urlToImage }} />
          <View style={styles.conText}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Detail', {
                  urlLink: data.url
                  })}>
                  <Text style={styles.textTitle}>{data.title}</Text>
              </TouchableOpacity>
           
            <Text>{data.author}</Text>
            <View style={styles.conSumber}>
              <Text style={{ color: "#aaaaaa" }}>{data.source.name}</Text>
              <Text style={{ color: "#aaaaaa" }}>{dateNews}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  conCard: {
    borderWidth: 1,
    borderColor: "#dedede",
    marginVertical: 10,
    borderRadius: 8,
    shadowColor: "#697E99",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 8.65,

    elevation: 2,
  },
  imageCard: {
    width: "100%",
    height: 200,
    resizeMode: "cover",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  conText: {
    flexDirection: "column",
    backgroundColor: "#fff",
    padding: 20,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  conSumber: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  textTitle: {
    fontSize: 20,
    fontWeight: "700",
    marginBottom: 5,
  },
  textAuthor: {
    color: "#aaaaaa",
  },
});
