import React from 'react'
import { connect } from 'react-redux'
import apiCall from '../../Store/ActionCreator'
import {View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import Card from './Card'


class HomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data:''
        }
    }

    componentDidMount(){
        this.props
        .apiCall("https://newsapi.org/v2/top-headlines?country=id&apiKey=54abb45a32314dc485cf180629b23189")
        .then(() => {
            const data = this.props.data;
            this.setState({
                data,
            })
        })
        .catch(error => {
            console.log(error)
        })
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.conTextHome}>
                    <Text style={styles.home}>News21</Text>
                    <Text style={styles.homeShadow}>Home</Text>
                </View>
             
                <FlatList
                data={this.props.data.articles}
                renderItem={({ item }) =>(
                 <Card data={item} navigation ={this.props.navigation} />                  
                )}
                keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }
}



const mapDispatchToProps = dispatch => ({
    apiCall: url => dispatch(apiCall(url))
})

const mapStateToProps = state => ({
    data: state.apiReducer.data,
    error: state.apiReducer.error
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#f5f6f8',
        fontFamily:'Roboto',
        paddingTop: 16,
        paddingHorizontal: 16
    },
    conTextHome: {
        paddingVertical: 20
    },
    home: {
        fontSize: 70,
        fontWeight:'700',
        position:'relative',
        color: 'rgba(0,0,0, .68)'
    },
    homeShadow: {
        fontSize: 90,
        position: 'absolute',
        top: 10,
        fontWeight:'900',
        color: 'rgba(0, 0, 0, .10)'
    }
})