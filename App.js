import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Store from './src/Store/Store'
import {Provider} from 'react-redux'

import Appsrc from './src/View/index'

export default function App() {
  return (
    <Provider store={Store}>
      <Appsrc />
    </Provider>
  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
